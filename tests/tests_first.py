import unittest


class TestClass(unittest.TestCase):

    def test_sum(self):
        result = sum([1, 2, 3])
        self.assertEqual(result, 6)

    def test_sum111(self):
        result = sum([1, 2, 31])
        self.assertIsNot(result, 6)


if __name__ == "__main__":
    unittest.main()
