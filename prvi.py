from permutations import create_permutations, find_positions

startText = 'ABBAYABBAYABBA'
startText = startText + '$'

# permutation and starting position of the
# first character in permutation in the original text
permutations = [(startText, 0)]

# keeps tuples of first and the last character in permutation
# in should be sorted by the first element in the tuple - the first char. in permt.
first_last = []

create_permutations(startText, permutations)

print('After permutations')
for i in range(0, len(permutations)):
    print(permutations[i])

# take only the first and the last column
for i in range(0, len(permutations)):
    first_last.append((permutations[i][0][0], permutations[i][0][-1]))

print('Just first and last character')
print(first_last)

# store total number of each character
dict_char_total = dict()
# ranks of the characters in the last column
ranks_of_L_elems = []
# finish BW transformation
BWT = ''

for i in range(0, len(first_last)):
    character = first_last[i][1]
    BWT = BWT + character
    if character not in dict_char_total:
        dict_char_total[character] = 0
    ranks_of_L_elems.append(dict_char_total[character])
    dict_char_total[character] += 1

# sorted first column, but in such way that the same characters
# do not appear one after the other, but total number of
# characters says how many times does one character appear
sortTotal = sorted(dict_char_total.items(), key=lambda x: x[0])

print("sortTotal")
print(sortTotal)
print("totals")
print(dict_char_total)
print("ranks")
print(ranks_of_L_elems)
print('final Burrow-Wheeler transformation aka last column')
print(BWT)

reversed = '$'
currentChar = first_last[0][1]
currentCharRank = ranks_of_L_elems[0]
newChar = ''

while newChar != '$':
    reversed = currentChar + reversed
    stepsToSkip = 0;
    for i in range(1, len(sortTotal)):
        if sortTotal[i][0] != currentChar:
            stepsToSkip += sortTotal[i][1]
        else:
            break
    stepsToSkip += (currentCharRank + 1)

    newChar = first_last[stepsToSkip][1]
    currentChar = newChar
    currentCharRank = ranks_of_L_elems[stepsToSkip]

print('REVERSED')
print(reversed)

# import sys
# sys.stdout = open("C:\\Users\\albi\\result_file.txt", "w")

found_positions = find_positions(first_last, ranks_of_L_elems, dict_char_total, 'GTYABBA')

print("******    RESULTS:")
for i in range(0, len(found_positions)):
# found_position carries position in first column
# exact positions are at the position in permutation array, under the second element of tuple
    print(permutations[found_positions[i]][1])
    print(permutations[found_positions[i]][0])
print('Kraj')
