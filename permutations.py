def create_permutations(startText, permutations):
    for i in range(1, len(startText)):
        tempElem = permutations[-1][0]
        tempChar = tempElem[0]
        tempElem = tempElem[1:]
        tempElem = tempElem + tempChar
        permutations.append((tempElem, i))

    permutations[:] = sorted(permutations, key=lambda permutation: permutation[0])
    return


def find_positions(first_last, ranks_of_L_elems, dict_char_total, query):
    if query == '':
        return

    found_positions = []
    L_Char = query[-1]
    query_length = len(query)
    num_covered_chars = 0
    while (L_Char != '$') or (num_covered_chars != query_length):
        # search down the L column
        for i in range(0, len(first_last)):
            if first_last[i][1] == L_Char:
                # character is covered. Find next one
                # numCoveredChars = 1

                # position of the character among the same characters
                steps_to_skip = ranks_of_L_elems[i]
                j = 1
                while j < len(first_last):
                    F_Char = first_last[j][0]
                    if F_Char == L_Char:
                        num_covered_chars += 1
                        FL_position = j + steps_to_skip

                        L_Char = first_last[FL_position][1]
                        print(first_last[FL_position][0] + "" + str(steps_to_skip) + "-" + first_last[FL_position][1] + "" + str(ranks_of_L_elems[FL_position]))

                        # pre-check of characters from the end of query
                        index_from_back = -1-num_covered_chars
                        # does current_char match query sequence
                        if(num_covered_chars != query_length):
                            if (L_Char != query[index_from_back]):
                                print(L_Char + " does not match " + str(index_from_back) + " in " + query)

                                L_Char = query[-1]
                                num_covered_chars = 0
                                break
                            else:
                                print(L_Char + " matches " + str(index_from_back) + " in " + query)
                        # end before end of query sequence
                        elif L_Char == '$' and num_covered_chars != query_length:
                            L_Char = query[-1]
                            break
                        # end text or end of query sequence
                        elif L_Char == '$' or num_covered_chars == query_length:
                            # steps_to_skip - 1 because '$' is part of first_last array, but not original text
                            found_positions.append(FL_position)
                            L_Char = query[-1]
                            num_covered_chars = 0
                            break
                        else:
                            steps_to_skip = ranks_of_L_elems[FL_position]
                            j = 1
                            # numCoveredChars += 1
                    else:
                        j += dict_char_total[first_last[j][0]]

        #  returns to while
        break

    return found_positions
